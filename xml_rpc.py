from wordpress_xmlrpc import Client
from wordpress_xmlrpc import WordPressPost
from wordpress_xmlrpc.methods import posts
from data_extractor import ExtractData
import datetime, time
import calendar
import re
import sys

sys.setrecursionlimit(10000)


# endpoint = 'http://ec2-52-50-145-68.eu-west-1.compute.amazonaws.com/xmlrpc.php'
# admin_login = 'iram'
# admin_password = 'IraM25051993'

endpoint = 'http://www.agreementbase.com/xmlrpc.php'
admin_login = 'agreementbase_kgvnb0'
admin_password = 'new_p@ssw0rd_ydh362'


# endpoint = 'http://www.shaunspalding.com/test/xmlrpc.php'
# admin_login = 'shaunspalding_8cehzy'
# admin_password = 'eNZAacmYY)jM7D1Sw'

client = Client(endpoint, admin_login, admin_password)
print("[", datetime.datetime.now().time(), "] Starting crawler.")

with open("test_result.csv", 'r') as csv_file:
    url_index = 0

    scraped_urls_file = open("scraped_links.txt", 'a')

    for url in csv_file:
        url_index += 1
        print("\n\n", url_index, "Extracting data from ", url)

        extractor = ExtractData(url)
        extractor.ParsePage()

        title = extractor.getTitle()
        tags = extractor.getTags()
        rTags = extractor.getRTags()
        date = extractor.getDate()

        print("Title: ", title)
        print("Date: ", date)

        content = extractor.getContent()

        if content:
            retry = 3
            while retry > 0:
                try:
                    newpost = WordPressPost()
                    newpost.title = str(title)
                    newpost.content = content

                    # todo: clarify what date should be used
                    year = int(date.split(',')[1].strip()[:4])
                    month = int(list(calendar.month_abbr).index(date.split(' ')[0][:3]))
                    day = int(re.findall("(\d+)",  date.split(' ')[1])[0])
                    newpost.date = datetime.datetime(year, month, day)

                    # todo: clarify what should be status when posting “draft”, “private” or “publish”?
                    newpost.post_status = 'publish'

                    newpost.terms_names = {
                        'post_tag': tags + rTags,
                    }

                    newpost.id = client.call(posts.NewPost(newpost))

                    scraped_urls_file.write("\n")
                    scraped_urls_file.write(url)
                    scraped_urls_file.write("title: ")
                    scraped_urls_file.write(title)
                    scraped_urls_file.write(" date: ")
                    scraped_urls_file.write(date)

                    break
                except Exception as e:

                    failed_links_file = open("failed_links.txt", 'a')
                    failed_links_file.write(url)
                    failed_links_file.close()
                    print('error:', e)
                    if str(e) != "<Fault 500: 'A term with the name provided already exists in this taxonomy.'>":
                        time.sleep(120)
                        retry -= 1
                    else:
                        retry = 0

        else:
            print("Empty content for url ", extractor.getURL())

    scraped_urls_file.close()
    print("[", datetime.datetime.now().time(), "] Finished crawler.")
