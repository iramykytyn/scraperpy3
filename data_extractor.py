from bs4 import BeautifulSoup, Tag
import urllib.request
import gzip, io
import random
import time
import re

base_url = "https://www.lawinsider.com"
user_agents = list()

class ExtractData:

    def __init__(self, url):
        self.url = url
        self.title = ''
        self.date = ''
        self.content = ''
        self.tags = list()
        self.right_side_tags = list()

    def randomSleep(self):
        sleeptime = random.randint(60, 120)
        time.sleep(sleeptime)

    def clean_external_links(self):
        if self.content:
            soup = BeautifulSoup(str(self.content), 'html.parser')

            for link in soup.find_all('a'):
                del link['href']
                print(str(link))

    def getURL(self):
        return self.url

    def setURL(self, url):
        self.url = url

    def getTitle(self):
        return self.title

    def setTitle(self, title):
        self.title = title

    def getDate(self):
        return self.date

    def setDate(self, date):
        self.date = date.strip()

    def getContent(self):
        return self.content

    def setContent(self, content):
        self.content = content

    def getTags(self):
        return self.tags

    def setTags(self, tags):
        self.tags = tags

    def getRTags(self):
        return self.right_side_tags

    def setRTags(self, tags):
        self.right_side_tags = tags

    def GetHtml(self):
        print("Getting html from url...")

        retry = 5
        while retry > 0:
            try:
                request = urllib.request.Request(self.url)

                length = len(user_agents)
                index = random.randint(0, length - 1)
                user_agent = user_agents[index]
                request.add_header('User-agent', user_agent)
                request.add_header('connection', 'keep-alive')
                request.add_header('Accept-Encoding', 'gzip')
                request.add_header('referer', base_url)

                print("Send request...")
                response = urllib.request.urlopen(request)

                html = response.read()

                if (response.headers.get('content-encoding', None) == 'gzip'):
                    print("gziping...")
                    html = gzip.GzipFile(fileobj=io.BytesIO(html)).read()

                return html

                break

            except urllib.error.HTTPError as e:
                print('url error:', e)
                self.randomSleep()
                retry -= 1
                failed_links_file = open("failed_links.txt", 'a')
                failed_links_file.write(self.url)
                failed_links_file.close()
                continue

            except Exception as e:
                print("error:", e)
                retry -= 1
                self.randomSleep()
                failed_links_file = open("failed_links.txt", 'a')
                failed_links_file.write(self.url)
                failed_links_file.close()
                continue

    def ParsePage(self):
        print("Parsing...")

        html = self.GetHtml()

        if not html:
            print("Error. Got empty html for url ", self.url)

        soup = BeautifulSoup(html, 'html.parser')

        title = soup.find('h1').getText()
        self.setTitle(title)

        for tags in soup.find_all('a', attrs={'class': 'tag'}):
            tag = tags.string
            tag_str = str(tag)
            re.sub(' +', ' ', tag_str)
            self.tags.append(str(tag_str))

        for sidebar_box in soup.find_all('div', attrs={'class': 'sidebar'}):

            if sidebar_box.find('h4').getText() == 'Items found in this contract':

                for sub_category in sidebar_box.find_all('a'):

                    sub_cat_text = sub_category.getText()

                    if not sub_cat_text:
                        continue

                    sub_cat_text_str = str(sub_cat_text)
                    sub_cat_text_str = re.sub('\xa0+', ' ', sub_cat_text_str)
                    sub_cat_text_str = re.sub(' +', ' ', sub_cat_text_str)

                    self.right_side_tags.append(sub_cat_text_str)

        date = soup.find('span', attrs={'class': 'date'})
        self.setDate(date.getText())

        content = soup.find('div', attrs={'class': 'row contract-content'})


        for link in content.find_all('a'):
            try:
                link_str = str(link)
                if not re.match('<a[^>]* href="[#]', link_str):
                    if link.string and link.previousSibling and link.previousSibling.string != "":
                        link.previousSibling.replaceWith(link.previousSibling + link.string)
                        link.extract()
                    elif link.string:
                        link.replaceWith(link.string)
                    else:
                        link.extract()
            except:
                print("Exception occured.")
                failed_links_file = open("failed_links.txt", 'a')
                failed_links_file.write(self.url)
                failed_links_file.close()

        content_str = str(content)
        pre_tag = content.find('pre')
        if pre_tag:
            sibl_tag = pre_tag.nextSibling
            while sibl_tag:

                pre_tag.append(sibl_tag)
                sibl_tag = pre_tag.nextSibling
        else:
            content_str = content_str.replace('\n', '')
        self.setContent(content_str)

def load_user_agent():
    fp = open('./user_agents.txt', 'r')

    line = fp.readline().strip('\n')

    while (line):
        user_agents.append(line)
        line = fp.readline().strip('\n')

    fp.close()

load_user_agent()


